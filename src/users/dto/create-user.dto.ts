import { IsNotEmpty, MinLength, Matches } from 'class-validator';
export class CreateUserDto {
  @MinLength(5)
  @IsNotEmpty()
  login: string;

  @MinLength(5)
  @IsNotEmpty()
  name: string;

  @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[^a-zA-Z0-9])(?!.*\s).{7,15}$/)
  @MinLength(8)
  @IsNotEmpty()
  password: string;
}
