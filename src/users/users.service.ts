import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
let users: User[] = [
  { id: 1, login: 'admin', name: 'Administrator', password: 'Pass@1412' },
  { id: 2, login: 'user1', name: 'User 1', password: '@1412' },
  { id: 3, login: 'user2', name: 'User 2', password: '@1412' },
];
let lastId = 4;
@Injectable()
export class UsersService {
  create(createUserDto: CreateUserDto) {
    const newUser: User = {
      id: lastId++,
      ...createUserDto,
    };
    users.push(newUser);

    return newUser;
  }

  findAll() {
    return users;
  }

  findOne(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return users[index];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateUser: User = {
      ...users[index],
      ...updateUserDto,
    };
    users[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const DeleteUser = users[index];
    users.splice(index, 1);
    return DeleteUser;
  }
  reset() {
    users = [
      { id: 1, login: 'admin', name: 'Administrator', password: 'Pass@1412' },
      { id: 2, login: 'user1', name: 'User 1', password: '@1412' },
      { id: 3, login: 'user2', name: 'User 2', password: '@1412' },
    ];
    lastId = 4;
    return 'RESET';
  }
}
