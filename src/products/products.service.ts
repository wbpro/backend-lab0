import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common/exceptions';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
const Products: Product[] = [
  {
    id: 1,
    name: 'คาปูชิโน',
    price: 40,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061580763423834161/45.Iced-Cappuccino1080-removebg-preview.png',
  },
  {
    id: 2,
    name: 'ลาเต้',
    price: 40,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061581019062468678/121.Latte_Iced_Reserve_BlackEG1080-removebg-preview.png',
  },
  {
    id: 3,
    name: 'มอคค่า',
    price: 40,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061580297398931506/90.Espresso-Frappuccino1080-removebg-preview.png',
  },
  {
    id: 4,
    name: 'เอสเพรสโซ่',
    price: 40,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061681130790723654/90.Espresso-Frappuccino1080-removebg-preview.png',
  },
  {
    id: 5,
    name: 'อเมริกาโน่',
    price: 40,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061681486354456607/40.Iced-Caffe-Americano1080-removebg-preview.png',
  },
  {
    id: 6,
    name: 'ชาเขียวนม',
    price: 40,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061682630816444516/dad3a8a7cf06417088c93d03e58c8eb0-removebg-preview.png',
  },
  {
    id: 7,
    name: 'มัทฉะลาเต้',
    price: 40,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061683494562050048/Starbucks2-removebg-preview.png',
  },
  {
    id: 8,
    name: 'ชาเย็น',
    price: 40,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061683494188761168/-cover-content-WEB-removebg-preview.png',
  },
  {
    id: 9,
    name: 'โกโก้',
    price: 40,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061683493815459980/RD0198_ImageBannerMobile_960x633_New_-01-scaled-removebg-preview.png',
  },
  {
    id: 10,
    name: 'นมสด',
    price: 40,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061684840556146748/86.Iced_Milk1080-removebg-preview.png',
  },
  {
    id: 11,
    name: 'แฟลทไวท์',
    price: 45,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061684840216399992/14.Flat-white1080-removebg-preview.png',
  },
  {
    id: 12,
    name: 'โอริโอ้',
    price: 45,
    category: 'drink',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061684839822143638/image-removebg-preview_3.png',
  },
  {
    id: 13,
    name: 'แพนเค้ก',
    price: 65,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/746351727867068437/1061704562601381888/image-removebg-preview_5.png',
  },
  {
    id: 14,
    name: 'ครัวซองต์',
    price: 55,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061685281373298870/INTHANIN-059_copy.png',
  },
  {
    id: 15,
    name: 'บานอฟฟี่พาย',
    price: 60,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/771011951450259509/1061688847244726374/31.Banoffee-Pie1080-1-removebg-preview.png',
  },
  {
    id: 16,
    name: 'บลูเบอรี่ชีสเค้ก',
    price: 45,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061685841338060901/lovepik-blueberry-cheesecake-png-image_402005532_wh860-removebg-preview.png',
  },
  {
    id: 17,
    name: 'เฟรนช์โทส',
    price: 75,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/771011951450259509/1061693195429351565/Simply-Recipes-French-Toast-Lead-Shot-3b-c3a68a576a9548f5bd43cce3d2d7f4b7-removebg-preview.png',
  },
  {
    id: 18,
    name: 'วาฟเฟิล',
    price: 55,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/746351727867068437/1061706253568593920/image-removebg-preview.png',
  },
  {
    id: 19,
    name: 'ป๊อบคอร์นคาราเมล',
    price: 35,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/746351727867068437/1061706837382148176/image-removebg-preview.png',
  },
  {
    id: 20,
    name: 'คัสตาร์ดพุดดิ้ง',
    price: 50,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061687227127701505/kisspng-chocolate-pudding-tiramisu-crme-caramel-cake-postres-mabes-pasteleria-5ba3d8c0d8a349.4647358915374645128874-removebg-preview.png',
  },
  {
    id: 21,
    name: 'ทีรามิสุ',
    price: 65,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/771011951450259509/1061694011896778803/Tiramisu-11484-removebg-preview.png',
  },
  {
    id: 22,
    name: 'เค้กโรล',
    price: 40,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061688171412336730/pngtree-towel-roll-cake-dessert-png-image_3924689-removebg-preview.png',
  },
  {
    id: 23,
    name: 'เครปเค้ก',
    price: 55,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/746351727867068437/1061708562130276535/image-removebg-preview.png',
  },
  {
    id: 24,
    name: 'บราวนี่',
    price: 35,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061688170753835028/kisspng-chocolate-brownie-chocolate-chip-cookie-fudge-blon-cocoa-5ac39632e90b30.5748301615227674109546-removebg-preview.png',
  },
  {
    id: 25,
    name: 'คัพเค้ก',
    price: 25,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/746351727867068437/1061707296088014929/image-removebg-preview.png',
  },
  {
    id: 26,
    name: 'คุกกี้',
    price: 25,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/746351727867068437/1061709301955170395/image-removebg-preview.png',
  },
  {
    id: 27,
    name: 'ชีสเค้ก',
    price: 55,
    category: 'dessert',
    img: 'https://cdn.discordapp.com/attachments/746351727867068437/1061707929323065354/image-removebg-preview.png',
  },
  {
    id: 28,
    name: 'ไข่กะทะ',
    price: 35,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061601495126118490/image-removebg-preview.png',
  },
  {
    id: 29,
    name: 'สปาเกตตีคาโบนาร่า',
    price: 75,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061584233484791849/IMG_0359.png',
  },
  {
    id: 30,
    name: 'สปาเกตตีซอสหมู',
    price: 75,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061585006415314954/IMG_0361.png',
  },
  {
    id: 31,
    name: 'พิซซ่าขนมปังโฮลวีต',
    price: 55,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061686496731611166/1405269758-002resizer-o-removebg-preview.png',
  },
  {
    id: 32,
    name: 'มินิพิซซ่าแฮมชีส',
    price: 55,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/746351727867068437/1061709788377002004/image-removebg-preview.png',
  },
  {
    id: 33,
    name: 'แซนด์วิชไข่กวน',
    price: 45,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/746351727867068437/1061710886521274368/image-removebg-preview.png',
  },
  {
    id: 34,
    name: 'แซนด์วิชไก่กรอบ',
    price: 50,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/746351727867068437/1061710224446214184/image-removebg-preview.png',
  },
  {
    id: 35,
    name: 'สเต๊กไก่',
    price: 89,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061591509809504276/IMG_0370.png',
  },
  {
    id: 36,
    name: 'สเต๊กแซลม่อน',
    price: 159,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061689545491501137/ec790850139642c282140f77cf3b8e0b-removebg-preview.png',
  },
  {
    id: 37,
    name: 'มันฝรั่งอบชีสเบคอน',
    price: 75,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061691527472099378/image-removebg-preview_4.png',
  },
  {
    id: 38,
    name: 'ผักโขมอบชีส',
    price: 70,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061691823443152968/141-removebg-preview.png',
  },
  {
    id: 39,
    name: 'สลัดทูน่า',
    price: 79,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061593594655408198/IMG_0371.png',
  },
  {
    id: 40,
    name: 'ข้าวไข่เจียว',
    price: 40,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061594022533148702/IMG_0372.png',
  },
  {
    id: 41,
    name: 'ข้าวหมูทอดทงคัตสึ',
    price: 55,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061692275777884290/1642146183_030d7e8e966169ab4c7f67c291c333f4-removebg-preview.png',
  },
  {
    id: 42,
    name: 'ข้าวไข่ข้นกุ้ง',
    price: 50,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061590024950382612/IMG_0369.png',
  },
  {
    id: 43,
    name: 'ข้าวยำไก่แซ่บ',
    price: 55,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061692275417161758/o0og-hero-removebg-preview.png',
  },
  {
    id: 44,
    name: 'เฟรนฟราย',
    price: 45,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061589109623238706/IMG_0367.png',
  },
  {
    id: 45,
    name: 'นักเก็ตไก่',
    price: 50,
    category: 'food',
    img: 'https://cdn.discordapp.com/attachments/896296119095660576/1061589604655968346/IMG_0368.png',
  },
];
let lastId = 46;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastId++,
      ...createProductDto,
    };
    Products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return Products;
  }

  findOne(id: number) {
    const index = Products.findIndex((Products) => {
      return Products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }

    return Products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = Products.findIndex((Products) => {
      return Products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...Products[index],
      ...updateProductDto,
    };
    Products[index] = updateProduct;
    return Products[index];
  }

  remove(id: number) {
    const index = Products.findIndex((Products) => {
      return Products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const DeleteProduct = Products[index];
    Products.splice(index, 1);
    return DeleteProduct;
  }
}
