export class CreateProductDto {
  name: string;
  category: string;
  img: string;
  price: number;
}
